import { Component, For } from "solid-js"
import Seed0 from "@/assets/res/Seed0.svg"
import Seed1 from "@/assets/res/Seed1.svg"
import Seed2 from "@/assets/res/Seed2.svg"
import Seed3 from "@/assets/res/Seed3.svg"
import Seed4 from "@/assets/res/Seed4.svg"
import SeedError from "@/assets/res/SeedError.svg"

const AppSeed: Component<{ seed: String }> = (props) => {
    const getFlowerSVG = (seedNumber: String) => {
        switch (seedNumber) {
            case "0": {
                return Seed0
            }
            case "1": {
                return Seed1
            }
            case "2": {
                return Seed2
            }
            case "3": {
                return Seed3
            }
            case "4": {
                return Seed4
            }
            default: {
                return SeedError
            }
        }
    }

    return (
        <div class="border-gray  max-w-xl rounded-lg border-2 border-solid px-10 py-2">
            {/* <button><i class=""></i></button> */}
            <div class="break-arrow grid  grid-cols-2 gap-y-2 sm:grid-cols-4">
                <For each={props.seed.split("")}>
                    {
                        (seedNumber) => (
                            <div class="border-gray m-2 h-24 w-24 rounded-lg border-2 border-solid p-2">
                                {getFlowerSVG(seedNumber)}
                            </div>
                        )
                        // <i class="fa fa-arrow-right"></i>
                    }
                </For>
            </div>
        </div>
    )
}

export default AppSeed
