import { Component } from "solid-js"

export const ErrorPlaceholder: Component = (props) => {
    return (
        <div
            class="flex flex-col gap-5"
            style={{ "text-align": "center", margin: "10em 0" }}
        >
            <span class="font-bold">Shit hit the fan!</span>
            <p>
                We are probably cleaning up right now. Try again in some
                minutes.
            </p>
            <a class="btn btn-secondary" href=".">
                <i
                    class="fa-solid fa-rotate-right"
                    style={{ "margin-right": "0.5em" }}
                ></i>
                Try again
            </a>
        </div>
    )
}
